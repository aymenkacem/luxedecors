var elixir   = require('laravel-elixir');

require('./elixir-extensions');

elixir(function(mix) {
    mix
        .sass('main.scss', 'public/css/main.css')
        .babel([
          //'facebookUtils.js',
          'main.js'
        ], 'public/js/main.js')
        .scripts([
            // bower:js
            'jquery/dist/jquery.js',
            'bootstrap-sass/assets/javascripts/bootstrap.js',
            'sweetalert/dist/sweetalert.min.js',
            // endbower
        ], 'public/js/vendor.js', 'node_modules')
        .styles([
            // bower:css
            'sweetalert/dist/sweetalert.css',
            // endbower
        ], 'public/css/vendor.css', 'node_modules')
        .version(['css/main.css', 'js/main.js'])
        .copy('public/fonts', 'public/build/fonts')
        .images()
        //.copy('node_modules/font-awesome/fonts', 'public/fonts')
        .wiredep({
            //exclude: ['jquery']
        })
        .browserSync({
            proxy: process.env.APP_URL
        });
});
