<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/', 'AppController@home');
// Route::match(['get', 'post'], '/', 'AppController@home');
// Route::get('test', 'AppController@test');

//Route::post('auth/facebookLogin/{offline?}', 'Auth\AuthController@facebookLogin');
// Route::post('signup', 'AppController@signup');

/*Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);*/

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['api']], function () {
    Route::post('auth/login', 'Auth\AuthController@authenticate');

    Route::get('home', 'AppController@home');
    Route::post('auth/login', 'Auth\AuthController@authenticate');
    Route::get('getSettings', 'AppController@getSettings');
    Route::get('loadData', 'AppController@loadData');
    Route::get('getGallery', 'AppController@getGallery');
    Route::get('productList/{id}/{page?}', 'AppController@productList');
    Route::get('productDetail/{id}/{slug}', 'AppController@productDetail');
    Route::post('processCheckout', 'AppController@processCheckout');
    Route::get('gpgCheckout', 'AppController@gpgCheckout');
});
