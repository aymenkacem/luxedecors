<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function dashboard(Request $request)
    {
        $status = $request->input('status', 'pending');
        $clients = Client::where(['status' => $status])->paginate(15);

        $count['approved'] = Client::where(['status' => 'approved'])->count();
        $count['refused'] = Client::where(['status' => 'refused'])->count();
        $count['pending'] = Client::where(['status' => 'pending'])->count();

        return view('admin.dashboard', ['clients' => $clients, 'status' => $status, 'count' => $count]);
    }
}
