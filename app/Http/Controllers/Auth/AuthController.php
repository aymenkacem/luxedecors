<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;

use JWTAuth;
use JWTFactory;
use Tymon\JWTAuth\Exceptions\JWTException;

use Facebook;
use App\Participant;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function authenticate(Request $request, $offline = null)
    {
        $customClaims = ['foo' => 'bar'];
        $payload = JWTFactory::make($customClaims);
        $token = JWTAuth::encode($payload);
        //echo($token);
        $user = [
            'first_name' => 'Aymen',
            'last_name' => 'Kacem',
            'email' => 'aymen.kacem@me.com',
            'company' => 'Access Leader',
            'address' => 'Rue de la république',
            'city' => 'Hammam Sousse',
            'state' => '320',
            'postal_code' => '4011',
            'phone' => '54138535',
            'mobile' => '71222222',
            'newsletter' => true,
            'representative' => true
        ];
        return response()->json(['token' => $token->get(), 'user' => $user]);
        return response()->json(['error' => 'invalid_credentials', 'email' => 'Vérifiez votre courriel'], 401);
        return response()->json(['error' => 'could_not_create_token'], 500);

//        $credentials = $request->only('email', 'password');
//        try {
//            // verify the credentials and create a token for the user
//            if (! $token = JWTAuth::attempt($credentials)) {
//                return response()->json(['error' => 'invalid_credentials'], 401);
//            }
//        } catch (JWTException $e) {
//            // something went wrong
//            return response()->json(['error' => 'could_not_create_token'], 500);
//        }
//        // if no errors are encountered we can return a JWT
//        return response()->json(compact('token'));
//
//
//        if ($offline) {
//            $facebook_id = '129795977365516';
//            $name = 'Offline User';
//            $email = 'test@email.com';
//            $gender = 'male';
//        } else {
//            $fb = new Facebook\Facebook([
//                'app_id' => config('services.facebook.client_id'),
//                'app_secret' => config('services.facebook.client_secret'),
//                'default_graph_version' => 'v2.5',
//                'default_access_token' => $request->getContent(),
//            ]);
//
//            try {
//                $response = $fb->get('/me?fields=id,name,gender,email');
//            } catch(Facebook\Exceptions\FacebookResponseException $e) {
//                return response('Graph returned an error: ' . $e->getMessage(), 500);
//            } catch(Facebook\Exceptions\FacebookSDKException $e) {
//                return response('Facebook SDK returned an error: ' . $e->getMessage(), 500);
//            }
//
//            $user        = $response->getGraphUser();
//            $facebook_id = $user->getId();
//            $name        = $user->getName();
//            $email       = $user->getProperty('email');
//            $gender      = $user->getProperty('gender');
//        }
//
//        $user = User::where('facebook_id', $facebook_id)->first();
//
//
//        if ( $user ) {
//            $customClaims = ['alreadyExists' => true];
//        } else {
//            $user = new User;
//            $user->facebook_id = $facebook_id;
//            $user->name        = $name;
//            $user->email       = $email;
//            $user->gender      = $gender;
//            $user->ip          = $request->ip();
//
//            if ( ! $user->save() ) {
//                return response()->json(['error' => 'could_not_create_user'], 500);
//            }
//            $customClaims = ['alreadyExists' => false];
//        }
//
//        try {
//            $token = JWTAuth::fromUser($user, $customClaims);
//        } catch (JWTException $e) {
//            return response()->json(['error' => 'could_not_create_token'], 500);
//        }
//
//        return response()->json(['token' => $token, 'alreadyExists' => $customClaims['alreadyExists'] ]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
}
