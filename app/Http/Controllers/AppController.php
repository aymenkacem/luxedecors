<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Goutte\Client;
use DB;
use Symfony\Component\DomCrawler\Crawler;
use App\Cache;

class AppController extends Controller
{
    public function loadData(Request $request)
    {
        $data = DB::table('caches')->select('name', 'value')->where('updated_at', '>', $request->input('date', '0000-00-00 00:00'))->get();
        return response()->json(['date' => date('Y-m-d H:i:s'), 'data' => $data]);
        // TODO ajouter le nombre de requete fait
    }

    public function getSettings() {
        $data = [];
        $client = new Client();
        $crawler = $client->request('GET', 'https://www.luxedecors.com.tn');
        $crawler->filter('.main-menu>li')->each(function (Crawler $node) use (& $data) {
            $elem = $node->filter('a');
            $item['id'] = preg_replace('/.*\/index\/(\d+)/i', '${1}', $elem->attr('href'));
            $item['name'] = trim($elem->text());
            if ($node->filter('ul')->first()->count()) {
                $node->filter('ul')->first()->children()->each(function (Crawler $node) use (& $item) {
                    $elem = $node->filter('a');
                    $item2['id'] = preg_replace('/.*\/index\/(\d+)/i', '${1}', $elem->attr('href'));
                    $item2['name'] = trim($elem->text());
                    if ($node->filter('img')->count()) {
                        $item2['img'] = 'https://www.luxedecors.com.tn' . trim($node->filter('img')->attr('src'));
                    }
                    if ($node->filter('ul')->first()->count()) {
                        $node->filter('ul')->first()->children()->each(function (Crawler $node) use (& $item2) {
                            $elem = $node->filter('a');
                            $item3['id'] = preg_replace('/.*\/index\/(\d+)/i', '${1}', $elem->attr('href'));
                            $item3['name'] = trim($elem->text());
                            if ($node->filter('img')->count()) {
                                $item3['img'] = 'https://www.luxedecors.com.tn' . trim($node->filter('img')->attr('src'));
                            }
                            $item2['submenu'][] = $item3;
                        });
                    }
                    $item['submenu'][] = $item2;
                });
            }
            // TODO verifier le menu item en promotion
            $data['menu'][] = $item;
        });
        unset($data['menu'][0]);

        $crawler->filter('.footer-links>li')->each(function (Crawler $node) use (& $data) {
            $elem = $node->filter('a');
            //$item['id'] = preg_replace('/.*\/index\/(\d+)/i', '${1}', $elem->attr('href'));
            $item['name'] = trim($elem->text());
            $data['footerMenu'][] = $item;
        });

        $menu = Cache::firstOrNew(['name' => 'menu']);
        $menu->value = json_encode($data['menu']);
        $menu->save();

        $menu = Cache::firstOrNew(['name' => 'footerMenu']);
        $menu->value = json_encode($data['footerMenu']);
        $menu->save();

        return response()->json($data);
    }

    public function home()
    {
        $data = [];
        $client = new Client();
        $crawler = $client->request('GET', 'https://www.luxedecors.com.tn');
        $crawler->filter('.featured-products')->each(function(Crawler $node) use (& $data) {
            $item['id'] = preg_replace('/.*\/index\/(\d+)/i', '${1}', $node->filter('.header a')->attr('href'));
            //$item['name'] = trim($node->filter('.header')->text());
            $item['name'] = trim(str_replace('Toute la catégorie', '', $node->filter('.header')->text()));
            $node->filter('.touchcarousel-item')->each(function (Crawler $node) use (& $item) {
                $item2['id'] = preg_replace('/.*\/view_\/(\d+).*/i', '${1}', $node->filter('a')->attr('href'));
                $item2['slug'] = preg_replace('/.*\/view_\/\d+\/(.*)/i', '${1}', $node->filter('a')->attr('href'));
                $item2['price'] = trim($node->filter('.price')->first()->text());
                $item2['name'] = trim(str_replace($item2['price'], '', $node->text()));
                $item2['img'] = 'https://www.luxedecors.com.tn' . trim($node->filter('img')->attr('src'));
                $item['products'][] = $item2;
            });
            $data['featuredProducts'][] = $item;
        });

        $crawler->filter('#banner .rsContent')->each(function(Crawler $node) use (& $data) {
            $item['id'] = preg_replace('/.*\/product\/(\w+)/i', '${1}', $node->filter('a')->attr('href'));
            $item['img'] = trim($node->filter('img')->attr('src'));
            $data['banners'][] = $item;
        });

        $crawler->filter('#side-box-wrap a')->each(function(Crawler $node) use (& $data) {
            $item['id'] = preg_replace('/.*\/index\/(\w+)/i', '${1}', $node->attr('href'));
            $item['img'] = trim($node->filter('img')->attr('src'));
            $data['sidebox'][] = $item;
        });

        $menu = Cache::firstOrNew(['name' => 'home']);
        $menu->value = json_encode($data);
        $menu->save();

        return response()->json($data);
    }

    public function productList($id, $page)
    {
        $data = [];
        $client = new Client();
        $crawler = $client->request('GET', 'https://www.luxedecors.com.tn/fr/project/product/index/' . $id . '/' . $page);

        $crawler->filter('.products-grid li')->each(function (Crawler $node) use (& $data) {
            $item['id'] = preg_replace('/.*\/view_\/(\d+).*/i', '${1}', $node->filter('a')->attr('href'));
            $item['slug'] = preg_replace('/.*\/view_\/\d+\/(.*)/i', '${1}', $node->filter('a')->attr('href'));
            $item['price'] = trim($node->filter('.price')->first()->text());
            $item['name'] = trim(str_replace($item['price'], '', $node->text()));
            $item['img'] = 'https://www.luxedecors.com.tn' . trim($node->filter('img')->attr('src'));
            $data['products'][] = $item;
        });
        $crawler->filter('#breadcrumb li:not(.filter-btn-wrap)')->each(function (Crawler $node) use (& $data) {
            if ($node->filter('a')->count()) {
                $item['id'] = preg_replace('/.*\/index\/(\d+)/i', '${1}', $node->filter('a')->attr('href'));
                $item['name'] = trim($node->text());
                $data['breadcrumb'][] = $item;
            } else {
                $item['active'] = true;
                $item['name'] = trim($node->text());
                $data['breadcrumb'][] = $item;
            }
        });

        $crawler->filter('#sub-categ-panel ul li')->each(function (Crawler $node) use (& $data) {
            if ($node->filter('a')->count()) {
                $item['id'] = preg_replace('/.*\/index\/(\d+)/i', '${1}', $node->filter('a')->attr('href'));
                $item['name'] = trim($node->text());
                $data['subCateg'][] = $item;
            } else {
                $item['active'] = true;
                $item['name'] = trim($node->text());
                $data['subCateg'][] = $item;
            }
        });


        $pagenav = $crawler->filter('.pagenav');
        if ($pagenav->count()) {
            $pagenav->first()->children()->each(function (Crawler $node) use (& $data) {
                switch ($node->nodeName()) {
                    case 'span':
                        if ($node->filter('.icon-step-backward')->count()) {
                            $data['pagination']['first'] = 0;
                        } else {
                            $data['pagination']['last'] = preg_replace('/.*\/index\/\d+\/(\d+)/i', '${1}', $node->filter('a')->attr('href'));
                        }
                        break;
                    case 'i':
                        if ($node->filter('.icon-caret-left')->count()) {
                            $data['pagination']['prev'] = preg_replace('/.*\/index\/\d+\/(\d+)/i', '${1}', $node->filter('a')->attr('href'), -1, $count);
                            if (!$count) {
                                $data['pagination']['prev'] = 0;
                            }
                        } else {
                            $data['pagination']['next'] = preg_replace('/.*\/index\/\d+\/(\d+)/i', '${1}', $node->filter('a')->attr('href'));
                        }
                        break;
                    case 'b':
                    case 'a':
                        $name = $node->text();
                        $page = preg_replace('/.*\/index\/\d+\/(\d+)/i', '${1}', $node->attr('href'));
                        $data['pagination']['_'][] = compact('name', 'page');
                }
            });
        }

        $data['title'] = $crawler->filter('h1.header')->text();
        $data['nbArticle'] = $crawler->filter('#nb-article')->html();

        $menu = Cache::firstOrNew(['name' => 'productList_' . $id . '_' . $page]);
        $menu->value = json_encode($data);
        $menu->save();

        return response()->json($data);
    }

    public function productDetail($id, $slug)
    {
        $data = [];
        $client = new Client();
        $crawler = $client->request('GET', 'https://www.luxedecors.com.tn/fr/project/product/view_/' . $id . '/' . $slug );
        $crawler->filter('#breadcrumb li')->each(function (Crawler $node) use (& $data) {
            if ($node->filter('a')->count()) {
                $item['id'] = preg_replace('/.*\/index\/(\d+)/i', '${1}', $node->filter('a')->attr('href'));
                $item['name'] = trim($node->text());
                $data['breadcrumb'][] = $item;
            } else {
                $item['active'] = true;
                $item['name'] = trim($node->text());
                $data['breadcrumb'][] = $item;
            }
        });
        $crawler->filter('#product-details .attribute')->each(function (Crawler $node) use (& $data) {
            $data['attributes'][] = $node->text();
        });
        $crawler->filter('#pictograms li')->each(function (Crawler $node) use (& $data) {
                $item['img'] = trim($node->filter('img')->attr('src'));
                $item['name'] = trim($node->filter('.name')->text());
                $item['desc'] = trim($node->filter('.tooltip')->text());
                $data['pictograms'][] = $item;

        });
        $crawler->filter('.products-carousel li')->each(function (Crawler $node) use (& $data) {
            $item['id'] = preg_replace('/.*\/view_\/(\d+).*/i', '${1}', $node->filter('a')->attr('href'));
            $item['slug'] = preg_replace('/.*\/view_\/\d+\/(.*)/i', '${1}', $node->filter('a')->attr('href'));
            $item['price'] = trim($node->filter('.price')->first()->text());
            $item['name'] = trim(str_replace($item['price'], '', $node->text()));
            $item['img'] = 'https://www.luxedecors.com.tn' . trim($node->filter('img')->attr('src'));
            $data['featuredProducts'][] = $item;
        });
        $crawler->filter('#gallery .rsImg')->each(function (Crawler $node) use (& $data) {
            $data['gallery'][] = 'https://www.luxedecors.com.tn' . $node->attr('data-rsbigimg');
        });
        $data['img'] = $data['gallery'][0];
        array_splice($data['gallery'],0,1);

        $data['id'] = $crawler->filter('#item_sku')->attr('value');
        $data['title'] = $crawler->filter('#product-details>h1')->text();
        $data['ref'] = $crawler->filter('.ref')->text();
        $data['backUrl'] = $crawler->filter('.ref')->text();
        if ($crawler->filter('.description')->count()) {
            $data['description'] = $crawler->filter('.description')->text();
        }
        $data['price'] = $crawler->filter('#price')->text();
        $data['stockQty'] = $crawler->filter('#stocked_product')->attr('value');
        // TODO laminage encadrement param

        $menu = Cache::firstOrNew(['name' => 'product_' . $id]);
        $menu->value = json_encode($data);
        $menu->save();

        return response()->json($data);
    }

    public function processCheckout()
    {
//        return response()->json(['first_name' => 'Vérifiez votre first-name'], 400);
        return response()->json(true);
    }

    public function gpgCheckout()
    {
        return view('gpgCheckout');
    }
}
